# CREATETEMPLATE

Creates a new [blueprint](../entities/blueprint.md)

## ARGUMENTS

Pass appropriate arguments to corresponding extrinsic of the asylum-blueprints pallet directly or via connection lib. Let's describe arguments as JSON scheme:

```json
{
  "max": {
    "type": "number",
    "description": "How many NFTs will ever belong to this collection. 0 for infinite."
  },
  "owner": {
    "type": "string",
    "description": "Owner's address, e.g. CpjsLDC1JFyrhm3ftC9Gs4QoyrkHKhZKtK7YqGTRFtTafgp. Can be address different from minter to assign ownership to other entity on creation."
  },
  "metadata": {
    "type": "string",
    "description": "HTTP(s) or IPFS URI. If IPFS, MUST be in the format of ipfs://hash"
  },
  "interpretations": {
      "type": Interpretation[],
      "description": "The list of supported interpretation types and interpretations for these types"
  }
}
```

## EXAMPLE

```json
{
  "max": 100,
  "issuer": "CpjsLDC1JFyrhm3ftC9Gs4QoyrkHKhZKtK7YqGTRFtTafgp",
  "symbol": "SWORD",
  "metadata": "ipfs://QmVgs8P4awhZpFXhkkgnCwBp4AdKRj3F9K58mCZ6fxvn3j",
  "interpretations": [
    {
      "src": "hash-of-pixel-2D-source",
      "tags": ["inventory-view", "style-pixel", "2D"],
      "metadata": "hash-of-pixel-2D-metadata"
    },
    {
      "src": "hash-of-anime-2D-source",
      "tags": ["style-anime", "2D"],
      "metadata": "hash-of-anime-2D-metadata"
    }
  ]
}
```