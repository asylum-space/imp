# ACCEPTITEMUPDATE

Accept all updated [proposed](submitblueprintchangeproposal.md) by [blueprint's](../entities/blueprint.md) owner after succesful [blueprint's update](updateblueprint.md). If not accepted, all updated will be in pending state.

## ARGUMENTS

Pass appropriate arguments to corresponding extrinsic of the asylum-blueprints pallet directly or via connection lib. Let's describe arguments as JSON scheme:

```json
{
  "blueprint-id": {
    "type": "number",
    "description": "The id of the blueprint"
  },
  "item-id": {
    "type": "number",
    "description": "The id of the item to be updated"
  }
}
```

## EXAMPLE

```json
{
  "blueprint-id": 42,
  "item-id": 453
}