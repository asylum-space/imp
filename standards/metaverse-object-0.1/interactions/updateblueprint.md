# UPDATETEMPLATE

Update a [blueprint](../entities/blueprint.md) according to [proposal](../entities/proposal.md)

## ARGUMENTS

Pass appropriate arguments to corresponding extrinsic of the asylum-blueprints pallet directly or via connection lib. Let's describe arguments as JSON scheme:

```json
{
  "blueprint-id": {
    "type": "number",
    "description": "The id of the blueprint to be destroyed"
  },
    "proposal-id": {
    "type": "number",
    "description": "The id of the proposal"
  }
}
```

## EXAMPLE

```json
{
  "blueprint-id": 42,
  "proposal-id": 314
}
```